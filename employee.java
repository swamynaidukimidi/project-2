package com.employee.management;


import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.ComponentScan;




@SpringBootApplication
@ComponentScan(basePackages ="com.employee.management")

public class Application 
{

	//logging
	
            static final Logger logger  = LogManager.getLogger(Application.class.getName());
	

  

        	public static void main(String[] args)

                {
		
                    logger.info("entered application");
		
                    SpringApplication.run(Application.class, args);

	
                }


}


//employee controler

package com.employee.management.controller;


import java.util.List;


import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PatchMapping
;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;


import com.employee.management.model.Employee;

import com.employee.management.service.EmployeeService;


@RestController
public class EmployeeController 
{
	
	static final Logger logger  = LogManager.getLogger(EmployeeController.class.getName());


        	@Autowired
	private EmployeeService employeeService;
	


	
// displaying list of all employees
	

    @GetMapping("/employees")
	
       public List<Employee> getAllEmployee(){
		return employeeService.getAllEmployees();
	}



	
// displaying employee by id
	
   @GetMapping("/employees/{id}")
	
   public Employee getEmployee(@PathVariable int id)
        {
		return employeeService.getEmployee(id);
	}
	


	
 // inserting employee


	@PostMapping("/employees")
	
      public void addEmployees(@RequestBody Employee employee)
        {
		employeeService.addEmployee(employee);

	}



  	
//updating employee by id

	
@PutMapping("/employees/{id}")
	
public void updateEmployee(@RequestBody Employee e, @PathVariable int id)
{
		employeeService.updateEmployee(e, id);
	
}


	
	
// deleting all employees



	@DeleteMapping("/employees")

	public void deleteAllEmployees()
{
		employeeService.deleteAllEmployees();
	}

	
// deleting employee by id
	
    @DeleteMapping("employees/{id}")
	
public void deleteEmployeeByID(@RequestBody Employee e, @PathVariable int id)
{
		employeeService.deleteEmployeeByID(id);
	}



	
// updating/ patching employee by id
	

@PatchMapping("employees/{id}")
	

public void patchEmployeeByID(@RequestBody Employee e, @PathVariable int id)
 {
		
employeeService.patchEmployee(e, id);

	}

}




//employee service
package com.employee.management.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.employee.management.model.Employee;

import com.employee.management.repository.EmployeeRepository;


// employee service class


@Service
public
 class EmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	



	// fetching all employees

	
public List<Employee> getAllEmployees()

{

		
List<Employee> emps = (List<Employee>)employeeRepository.findAl(); 
		
return emps;
	
}


	
	// fetching employee by id


	public Employee getEmployee(int id)
{
		return employeeRepository.findOne(id);
	
}

	
	// inserting employee


	public void addEmployee(Employee e)
 {
		
           employeeRepository.save(e);

}

	
	
          // updating employee by id


	public void updateEmployee(Employee emp, int id)
        {

		if(id == emp.getEmployeeID()) 
{
			
employeeRepository.save(emp);
		
}	
        }

	
	
       // deleting all employees

	
public void deleteAllEmployees()
{
		employeeRepository.deleteAll();
	
}

	

	// deleting employee by id

	public void deleteEmployeeByID(int id)

{
		employeeRepository.delete(id);
	}

	

	//patching/updating employee by id

	
public void patchEmployee(Employee emp, int id)
 {
		if(id == emp.getEmployeeID())
 {
			employeeRepository.save(emp);
		}

	}
}

// employee repository
package com.employee.management.repository;


import org.springframework.data.repository.CrudRepository;


import com.employee.management.model.Employee;


// interface extending crud repository




public interface EmployeeRepository extends CrudRepository<Employee, Integer>{
	
}


//employee

package com.employee.management.model;


import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.JoinColumn;

import javax.persistence.ManyToOne;

import javax.persistence.Table;

import javax.validation.constraints.NotNull;



@Entity
@Table (name = "employee")

public class Employee 
{

	@Id
	@Column(name="employee_id")
	
        @GeneratedValue(strategy = GenerationType.AUTO)
	private int employeeID;
	

	@Column(name="first_name")
	private String firstName;
	
	
        @Column(name="last_name")
	private String lastName;
	
	
        @NotNull
	@ManyToOne
	@JoinColumn(name="department_id")
	private Department department;
	

	
public Employee()
{
		
	
}
	
	
public Employee(String firstName, String lastName, Department department)
 {
		super();
	
	this.firstName = firstName;
	
	this.lastName = lastName;
		
        this.department = department;
	
}


	
	public int getEmployeeID()
                 {
                   return employeeID;
	
                 }


	
public void setEmployeeID(int employeeID) 
               {
		
              this.employeeID = employeeID;

            	}





	public String getFirstName()
                                  {
		
                                    return firstName;

	}


	
       public void setFirstName(String firstName) 
         {
		
            this.firstName = firstName;

	}

	
           public String getLastName()
            {
		
           return lastName;


	     }

	
public void setLastName(String lastName)
            {
		
             this.lastName = lastName;
	
}


	
@Override
	public String toString()
{
		return String.format("Employee employeeID = %d, firstName = %s, lastName = %s, department_ID= %d",                                         employeeID, firstName, lastName, department.getDepartment_ID());
	
}

	



public Department getDepartment() 
{
		return department;
	
}


	
public void setDepartment(Department department)
 {
		this.department = department;
	}
	


}